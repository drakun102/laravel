<?php

use App\Http\Controllers\BusController;
use App\Http\Controllers\PassengerController;
use App\Http\Controllers\BusPassengersController;
use App\Models\Passenger;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});
//bus routes
Route::get('buses',[BusController::class,'index'])->name('buses.index');
Route::get('buses/create',[BusController::class,'create'])->name('buses.create');
Route::post('buses',[BusController::class,'store'])->name('buses.store');
Route::get('buses/{id}/edit',[BusController::class,'edit'])->name('buses.edit');
Route::put('buses/{id}',[BusController::class,'update'])->name('buses.update');
Route::delete('buses/{id}',[BusController::class,'destroy'])->name('buses.destroy');

//passanger routes
Route::get('passengers',[PassengerController::class,'index'])->name('passengers.index');
Route::get('passengers/create',[PassengerController::class,'create'])->name('passengers.create');
Route::post('passengers',[PassengerController::class,'store'])->name('passengers.store');
Route::get('passengers/{id}/edit',[PassengerController::class,'edit'])->name('passengers.edit');
Route::put('passengers/{id}',[PassengerController::class,'update'])->name('passengers.update');
Route::delete('passengers/{id}',[PassengerController::class,'destroy'])->name('passengers.destroy');
//Buspassangers routes
Route::get('buspassengers',[BusPassengersController::class,'index'])->name('buspassengers.index');
Route::get('buspassengers/create',[BusPassengersController::class,'create'])->name('buspassengers.create');
Route::post('buspassengers',[BusPassengersController::class,'store'])->name('buspassengers.store');
Route::get('buspassengers/{id}/edit',[BusPassengersController::class,'edit'])->name('buspassengers.edit');
Route::put('buspassengers/{id}',[BusPassengersController::class,'update'])->name('buspassengers.update');
Route::delete('buspassengers/{id}',[BusPassengersController::class,'destroy'])->name('buspassengers.destroy');
