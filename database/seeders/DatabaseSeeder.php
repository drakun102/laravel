<?php

namespace Database\Seeders;

use App\Models\Bus;
use App\Models\Passenger;
use App\Models\User;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // User::factory(10)->create();

        User::factory()->create([
            'name' => 'Test User',
            'email' => 'test@example.com',
        ]);

        $Bus = new Bus();
        $Bus->name = 'Bus1';
        $Bus->status = 0;
        $Bus->passenger = 1;
        $Bus->save();
        $Passenger = new Passenger();
        $Passenger->name = 'Jose';
        $Passenger->busid = 1;
        $Passenger->save();
    }
}
