<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('buspassengers', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedBigInteger('id_bus')->nullable();
            $table->unsignedBigInteger('id_passenger')->nullable();

            //foreign keys
            $table->foreign('id_bus')
                    ->references('id')->on('buses')
                    ->onDelete('cascade');
            $table->foreign('id_passenger')
                        ->references('id')->on('passengers')
                        ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('buspassengers');
    }
};
