<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <label>index</label>
    <a href={{ route('passengers.create') }}>Crear</a>
    <ul>
        @foreach ($passengers as $passenger)
            <li><a href={{route('passengers.edit',$passenger->id)}}>{{$passenger->name}}</a></li>
        @endforeach
    </ul>
</body>
</html>