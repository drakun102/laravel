<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Buspassengers extends Model
{
    use HasFactory;
    public function bus(){

        return $this->belongsTo('App\Models\Bus');
    }
    public function passenger(){

        return $this->belongsTo('App\Models\Passenger');
    }
}
