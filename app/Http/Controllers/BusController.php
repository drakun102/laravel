<?php

namespace App\Http\Controllers;
use DB;
use App\Models\Bus;
use App\Models\Buspassengers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB as FacadesDB;

class BusController extends Controller
{
    public function index(){
        

    $buses = Bus::query()
    ->select('buses.*')
    ->addSelect(FacadesDB::raw('(select count(buspassengers.id_bus )  from buspassengers where buses.id = buspassengers.id_bus) as passenger'))
    ->get();
    $busesinservice  = BusPassengers::select('buspassengers.id','buspassengers.id_bus', 'buspassengers.id_passenger','buses.name','passengers.name as pname')
                ->join('buses', 'buspassengers.id_bus', '=', 'buses.id')
                ->join('passengers','buspassengers.id_passenger','=','passengers.id')
                ->where('buses.status', 1)
                ->count();
        return view('buses/index',compact('buses','busesinservice'));
    }
    public function create(){
        return view('buses/create');
    }
    public function store(Request $request){
        $bus = new Bus();

        $bus->name = $request->name;
        $bus->status = $request->status;
        // $bus->passenger = $request->passenger;
        $bus->save();
        return redirect()->route('buses.index');
    }
    public function edit($id){
        $bus = Bus::find($id);
        return view('buses.edit',compact('bus'));
    }
    public function update(Request $request ,$id){
        $bus = Bus::find($id);
        $bus->name = $request->name;
        $bus->status = $request->status;
        // $bus->passenger = $request->passenger;
        $bus->save();
        return redirect()->route('buses.index');
    }
    public function destroy(Request $request ,$id){
        $bus = Bus::find($id);
        $bus->delete();
        
        return redirect()->route('buses.index');

    }
}
