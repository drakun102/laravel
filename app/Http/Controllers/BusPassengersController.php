<?php

namespace App\Http\Controllers;

use App\Models\Bus;
use App\Models\BusPassengers;
use App\Models\Passenger;
use Illuminate\Http\Request;

class BusPassengersController extends Controller
{
    public function index(){
        
        $buspassengers = BusPassengers::select('buspassengers.id','buspassengers.id_bus', 'buspassengers.id_passenger','buses.name','passengers.name as pname')
                ->join('buses', 'buspassengers.id_bus', '=', 'buses.id')
                ->join('passengers','buspassengers.id_passenger','=','passengers.id')
                ->get();
        return view('buspassengers/index',compact('buspassengers'));
    }
    public function create(){
        $buses = Bus::all();
        $passengers = Passenger::select('passengers.id', 'passengers.name')
        ->whereNotIn('passengers.id',BusPassengers::select('buspassengers.id_passenger')->get()->toArray())
        ->get();
        return view('buspassengers/create',compact('buses','passengers'));
    }
    public function store(Request $request){
        $buspassengers = new BusPassengers();

        $buspassengers->id_bus = $request->idbus;
        $buspassengers->id_passenger = $request->idpassenger;
        // $bus->passenger = $request->passenger;
        $buspassengers->save();
        return redirect()->route('buspassengers.index');
    }
    public function edit($id){
        $buspassengers = BusPassengers::find($id);
        $buses = Bus::all();
        $passengers = Passenger::select('passengers.id', 'passengers.name')
        ->whereNotIn('passengers.id',BusPassengers::select('buspassengers.id_passenger')->get()->toArray())
        ->get();
        return view('buspassengers.edit',compact('buspassengers','buses','passengers'));
    }
    // public function update(Request $request ,$id){
    //     $bus = Bus::find($id);
    //     $bus->name = $request->name;
    //     $bus->status = $request->status;
    //     // $bus->passenger = $request->passenger;
    //     $bus->save();
    //     return redirect()->route('buses.index');
    // }

    public function destroy(Request $request ,$id){
        $buspassanger = BusPassengers::find($id);
        $buspassanger->delete();
        
        return redirect()->route('buspassengers.index');

    }
}
