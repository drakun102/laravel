<?php

namespace App\Http\Controllers;

use App\Models\Passenger;
use Illuminate\Http\Request;

class PassengerController extends Controller
{
    public function index(){
        $passengers = Passenger::all();
        return view('passengers/index',compact('passengers'));
    }
    public function create(){
        return view('passengers/create');
    }
    public function store(Request $request){
        $passenger = new Passenger();

        $passenger->name = $request->name;
        
        $passenger->save();
        return redirect()->route('passengers.index');
    }
    public function edit($id){
        $passenger = Passenger::find($id);
        return view('passengers.edit',compact('passenger'));
    }
    public function update(Request $request ,$id){
        $passenger = Passenger::find($id);
        $passenger->name = $request->name;
        
        $passenger->save();
        return redirect()->route('passengers.index');
    }

    public function destroy(Request $request ,$id){
        $passenger = Passenger::find($id);
        $passenger->delete();
        
        return redirect()->route('passengers.index');

    }
}
